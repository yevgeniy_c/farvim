-- last action = 1 ----- toggle visual mode
--             = 0 ----- sets after every Esc press, some kind of "clear this value"
Macro {
  description = "Toggle vim mode";
  area = "Shell Menu";
  key = "ShiftEsc"; -- Must be removed for something more ergonomic
  condition=function() return CmdLine.Empty and APanel.Visible end;

  action = function()
    if isModeOn == true then
      isModeOn = false;
    else isModeOn = true;
    end;
  end
}

Macro {
  description="Use vim-like search";
  area="Shell Menu"; key="/";
  condition=function()
    return CmdLine.Empty and APanel.Visible and isModeOn == true
  end;

  action=function()
      Keys('AltF7')
    end;
}

Macro {
  description="Use vim-like navigation";
  area="Shell Menu"; key="j";
  condition=function()
    return CmdLine.Empty and APanel.Visible and isModeOn == true
  end;

  action=function()

    if visualMode == true then
      if APanel.CurPos > visualModeStartPosition then
          Panel.Select(0, 1, 1, 0) -- select if current pos is lower than start point
      else
          if APanel.CurPos ~= visualModeStartPosition then
            Panel.Select(0, 0, 1, 0) -- remove selection if current pos is lower than start point
          end
    end;
  end;
  Panel.SetPosIdx(0, APanel.CurPos+1 )
  end;
}

Macro {
  description="Use vim-like navigation";
  area="Shell Menu"; key="k";
  condition=function()
    return CmdLine.Empty and APanel.Visible and isModeOn == true
  end;

  action=function()
    if visualMode == true then
      if APanel.CurPos < visualModeStartPosition then
          Panel.Select(0, 1, 1, 0)
      else
          if APanel.CurPos ~= visualModeStartPosition then
            Panel.Select(0, 0, 1, 0)
          end;
      end;
      --else
      --  Panel.Select(0, 2, 1, 0)
      --end;
  end;
  Panel.SetPosIdx(0, APanel.CurPos-1 )
  end;
}

Macro {
  description="Use vim-like navigation";
  area="Shell"; key="l";
  condition=function()
    return CmdLine.Empty and APanel.Visible and isModeOn == true
  end;

  action=function()
     Keys('Right')
  end;
}

Macro {
  description="Use vim-like navigation";
  area="Shell"; key="h";
  condition=function()
    return CmdLine.Empty and APanel.Visible and isModeOn == true
  end;

  action=function()
    Keys('Left')
  end;
}

Macro {
  description="Toggle something like visual mode in vim";
  area="Shell"; key="v";
  condition=function() return CmdLine.Empty and APanel.Visible and isModeOn == true end;

  action=function()
    if visualMode == true then
      if APanel.CurPos == visualModeStartPosition then
        Panel.Select(0, 2, 1, 0) -- remove selection from start point
      end
      visualMode  = false;
    else
      visualMode = true;
      Panel.Select(0, 1, 1, 0) -- select start position
      visualModeStartPosition = APanel.CurPos;
      end;
    lastAction = 1
  end;

}

Macro {
  description="Jump to the first position in panel";
  area="Shell"; key="g";
  condition=function() return CmdLine.Empty and APanel.Visible and isModeOn == true end;

  action=function()
    Panel.SetPosIdx(0, 1)
  end;
}

Macro {
  description="Jump to the last position in panel";
  area="Shell"; key="AltG";
  condition=function() return CmdLine.Empty and APanel.Visible and isModeOn == true end;

  action=function()
    Panel.SetPosIdx(0, APanel.ItemCount)
  end;
}


Macro {
  description="Clear selection"; -- Only for  now, it's general "shit, let's go back" key
  area="Shell"; key="Esc";
  condition=function() return CmdLine.Empty and APanel.Visible and isModeOn == true end;

  action=function()
    if lastAction == 1 then
      Panel.Select(0,0)
      visualMode = false
      lastAction = 0-- im too drunk to explain theis
    end
  end;
}


Macro {
  description="Rename file under cursor";
  area="Shell"; key="r";
  condition=function() return CmdLine.Empty and APanel.Visible and isModeOn == true end;

  action=function()
    Keys('ShiftF6')
  end;
}

Macro {
  description="Up on dir";
  area="Shell"; key="u";
  condition=function() return CmdLine.Empty and APanel.Visible and isModeOn == true end;

  action=function()
    Keys('CtrlPgUp')
  end;
}

Macro {
  description="Yank file or dir under cursor";
  area="Shell"; key="y";
  condition=function() return CmdLine.Empty and APanel.Visible and isModeOn == true end;

  action=function()
    Keys('F5')
  end;
}

Macro {
  description="Delete";
  area="Shell"; key="d";
  condition=function() return CmdLine.Empty and APanel.Visible and isModeOn == true end;

  action=function()
    Keys('F8')
  end;
}


Macro {
  description="Open file/Go to dir";
  area="Shell"; key="a";
  condition=function() return CmdLine.Empty and APanel.Visible and isModeOn == true end;

  action=function()
    Keys('Enter')
  end;
}

